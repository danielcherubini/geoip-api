module github.com/danielcherubini/geoip-api

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.48
	github.com/gorilla/mux v1.7.3
	github.com/oschwald/geoip2-golang v1.3.0
	github.com/oschwald/maxminddb-golang v1.5.0 // indirect
	golang.org/x/sys v0.0.0-20191206220618-eeba5f6aabab // indirect
)
